<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class Base extends Controller {
    
    public function log ($msg, array $array = array()) {
        $this->get('logger')->info($msg, $array);
    }
    
}
