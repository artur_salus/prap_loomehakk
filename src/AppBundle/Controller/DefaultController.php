<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Repository\Project\Project as ProjectRepo;

class DefaultController extends Base
{
    protected $projectrepo;
    
    public function __construct(ProjectRepo $repo) {
        $this->projectrepo = $repo;
    }
    
    public function indexAction(Request $request) {
        $projects = $this->projectrepo->search();
        
        return $this->render('AppBundle:Default:index.html.twig', array(
            'projects' => $projects,
            'today' => new \DateTime(),
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ));
    }
}
