<?php

namespace AppBundle\Pdf;

use Spraed\PDFGeneratorBundle\PDFGenerator\PDFGenerator;

class Service {

    protected $generator;
    
    public function __construct(PDFGenerator $gen) {
        $this->generator = $gen;
    }
    
    public function renderPdf($html, $encoding = 'UTF-8') {
        return $this->generator->generatePDF($html, $encoding);
    }

}
