<?php

namespace AppBundle\Repository\Project;

use AppBundle\Repository\Base;

class Project extends Base{
    
    public function search() {
        $qb = $this->createQueryBuilder('p');
        
        return $qb->getQuery()->getResult();
    }
    
}