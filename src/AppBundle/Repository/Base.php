<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\PaginatorInterface;

abstract class Base extends EntityRepository {

    protected $knp;

    public function setKnp(PaginatorInterface $knp) {
        $this->knp = $knp;
    }

    protected function knpPaginate($paginatable, $page = 1, $limit = 10) {
        return ($this->knp) ? $this->knp->paginate($paginatable, $page, $limit) : $paginatable;
    }

}