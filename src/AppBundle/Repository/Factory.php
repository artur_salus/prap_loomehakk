<?php

namespace AppBundle\Repository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class Factory {
    
    protected $em;
    protected $knp;
    
    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }
    
    public function setKnp(PaginatorInterface $knp) {
        $this->knp = $knp;
    }
    
    public function createRepo($entity) {
        $repo = $this->em->getRepository($entity);
        if($repo instanceof \AppBundle\Repository\Base && $this->knp) {
            $repo->setKnp($this->knp);
        }
        return $repo;
    }
    
}