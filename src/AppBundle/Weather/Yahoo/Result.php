<?php

namespace AppBundle\Weather\Yahoo;

class Result {

    protected $data;
    
    public function __construct(array $data) {
        $this->data = $data['query']['results']['channel'];
    }
    
    public function getTitle() {
        return $this->data['title'];
    }
    
    public function getLocation() {
        return implode(' ', $this->data['location']);
    }
    
    public function getWindData() {
        return $this->data['wind'];
    }
    
    public function getAtmosphereData() {
        return $this->data['atmosphere'];
    }
    
    public function getAstronomyData() {
        return $this->data['astronomy'];
    }
    
    public function getConditionData() {
        return $this->data['item']['condition'];
    }
    
    public function getForecastData() {
        return current($this->data['item']['forecast']);
    }
}
