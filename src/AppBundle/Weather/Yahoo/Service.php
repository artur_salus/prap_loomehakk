<?php

namespace AppBundle\Weather\Yahoo;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Project\Weather;

/**
 * @see https://developer.yahoo.com/weather/
 */

class Service {

    protected $url;
    protected $entityManager;
    private $weather;
    private $today;
    private $params;
    
    public function __construct($url, $entityManager) {
        $this->url = $url;
        $this->entityManager = $entityManager;
        $this->today = new \DateTime();
    }
    
    public function getCurrent($settlement) {
        if (!$this->validWeather()) {
            $this->weather = $this->fetchUrl($settlement);
            $this->updateWeather();
        }

        var_dump($this->validWeather());
    }

    public function fetchUrl($settlement) {
        $this->params = array(
            'q' => 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="'.$settlement.', EE")',
            'format' => 'json',
        );

        $url = $this->url.  http_build_query($this->params);
        $raw = file_get_contents($url);

        if(!$raw) {
            throw new \Exception('No data');
        } else {
            $data = json_decode($raw, true);
            if(!$data) {
                throw new \Exception('Invalid JSON');
            }
        }

        return new Result($data);
    }

    public function validWeather() {
        $data = $this->entityManager->getRepository('AppBundle\Entity\Project\Weather')->findOneBy(array('date' => $this->today));
        if (!isset($data)) { return false; } else { return unserialize($data->getData()); }
    }

    public function updateWeather() {
        $weather = new Weather();
        $weather->setData(serialize($this->weather));
        $weather->setDate($this->today);
        $this->entityManager->persist($weather);
        $this->entityManager->flush();
    }

}
