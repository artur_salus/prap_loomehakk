<?php

namespace AppBundle\Weather\Ilmateenistus;

class Result {

    protected $xml;
    
    public function __construct(\SimpleXMLElement $xml) {
        $this->xml = $xml;
    }
    
    public function getDayText() {
        return (string) $this->xml->day->text;
    }
    
    public function getNightText() {
        return (string) $this->xml->night->text;
    }

}
