<?php

namespace AppBundle\Weather\Ilmateenistus;

class Service {

    protected $url;
    
    public function __construct($url) {
        $this->url = $url;
    }
    
    public function getCurrent(\DateTime $dt = null) {
        $dt = ($dt) ? $dt : new \DateTime();
        $raw = file_get_contents($this->url);
        if(!$raw) {
            throw new \Exception('No data');
        }
        $xml = new \SimpleXMLElement($raw);
        $current = null;
        foreach($xml->forecast as $f) {
            $date = (string)$f[0]->attributes()->date;
            if($date == $dt->format('Y-m-d')) {
                $current = $f[0];
                break;
            }
            
        }
        
        if(!$current) {
            throw new \Exception('No value for date "'.$dt->format('Y-m-d').'"');
        }
        
        return new Result($current);
    }

}
