<?php

namespace AppBundle\Entity\Project;

class Event
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    /**
     * @var string
     */
    private $activity;

    /**
     * @var \AppBundle\Entity\Project\Location
     */
    private $location;

    /**
     * @var \AppBundle\Entity\Project\Person
     */
    private $supervisor;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Event
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Event
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set activity
     *
     * @param string $activity
     * @return Event
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Project\Location $location
     * @return Event
     */
    public function setLocation(\AppBundle\Entity\Project\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Project\Location 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set supervisor
     *
     * @param \AppBundle\Entity\Project\Person $supervisor
     * @return Event
     */
    public function setSupervisor(\AppBundle\Entity\Project\Person $supervisor = null)
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Get supervisor
     *
     * @return \AppBundle\Entity\Project\Person 
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Project\EventPerson $persons
     * @return Event
     */
    public function addPerson(\AppBundle\Entity\Project\EventPerson $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Project\EventPerson $persons
     */
    public function removePerson(\AppBundle\Entity\Project\EventPerson $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPersons()
    {
        return $this->persons;
    }
}
