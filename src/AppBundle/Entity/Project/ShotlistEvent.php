<?php

namespace AppBundle\Entity\Project;

class ShotlistEvent extends Event
{
    
    /**
     * @var integer
     */
    private $shot;

    /**
     * @var \AppBundle\Entity\Project\Shotlist
     */
    private $shotlist;


    /**
     * Set shot
     *
     * @param integer $shot
     * @return ShotlistEvent
     */
    public function setShot($shot)
    {
        $this->shot = $shot;

        return $this;
    }

    /**
     * Get shot
     *
     * @return integer 
     */
    public function getShot()
    {
        return $this->shot;
    }

    /**
     * Set shotlist
     *
     * @param \AppBundle\Entity\Project\Shotlist $shotlist
     * @return ShotlistEvent
     */
    public function setShotlist(\AppBundle\Entity\Project\Shotlist $shotlist = null)
    {
        $this->shotlist = $shotlist;

        return $this;
    }

    /**
     * Get shotlist
     *
     * @return \AppBundle\Entity\Project\Shotlist 
     */
    public function getShotlist()
    {
        return $this->shotlist;
    }
}
