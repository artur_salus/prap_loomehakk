<?php

namespace AppBundle\Entity\Project;

use Doctrine\ORM\Mapping as ORM;

/**
 * TehnicalList
 */
class TehnicalList
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $items;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TehnicalList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TehnicalList
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add items
     *
     * @param \AppBundle\Entity\Project\TehnicalListItem $items
     * @return TehnicalList
     */
    public function addItem(\AppBundle\Entity\Project\TehnicalListItem $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \AppBundle\Entity\Project\TehnicalListItem $items
     */
    public function removeItem(\AppBundle\Entity\Project\TehnicalListItem $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }
    /**
     * @var \AppBundle\Entity\Project\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\Project\User $user
     * @return TehnicalList
     */
    public function setUser(\AppBundle\Entity\Project\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Project\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
