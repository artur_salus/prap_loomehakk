<?php

namespace AppBundle\Entity\Project;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shotlist
 */
class Shotlist
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Shotlist
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add events
     *
     * @param \AppBundle\Entity\Project\ShotlistEvent $events
     * @return Shotlist
     */
    public function addEvent(\AppBundle\Entity\Project\ShotlistEvent $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \AppBundle\Entity\Project\ShotlistEvent $events
     */
    public function removeEvent(\AppBundle\Entity\Project\ShotlistEvent $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }
    /**
     * @var \AppBundle\Entity\Project\Callsheet
     */
    private $callsheet;


    /**
     * Set callsheet
     *
     * @param \AppBundle\Entity\Project\Callsheet $callsheet
     * @return Shotlist
     */
    public function setCallsheet(\AppBundle\Entity\Project\Callsheet $callsheet = null)
    {
        $this->callsheet = $callsheet;

        return $this;
    }

    /**
     * Get callsheet
     *
     * @return \AppBundle\Entity\Project\Callsheet 
     */
    public function getCallsheet()
    {
        return $this->callsheet;
    }
}
