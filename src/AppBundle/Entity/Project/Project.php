<?php

namespace AppBundle\Entity\Project;

/**
 * Project
 */
class Project {

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Project
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @var \AppBundle\Entity\Project\User
     */
    protected $user;

    /**
     * Set user
     *
     * @param \AppBundle\Entity\Project\User $user
     * @return Project
     */
    public function setUser(\AppBundle\Entity\Project\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Project\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @var string
     */
    protected $image;

    /**
     * @var \DateTime
     */
    protected $created;

    /**
     * Set image
     *
     * @param string $image
     * @return Project
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Project
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    public function getStart() {
        $start = null;
        foreach ($this->getCallsheets() as $callsheet) {
            foreach ($callsheet->getEvents() as $event) {
                if (!$start || $start > $event->getStart()) {
                    $start = $event->getStart();
                }
            }
            foreach ($callsheet->getShotlists() as $shotlist) {
                foreach ($shotlist->getEvents() as $event) {
                    if (!$start || $start > $event->getStart()) {
                        $start = $event->getStart();
                    }
                }
            }
        }
        
        if(!$start) {
            $start = $this->getCreated();
            $start->setTime(0, 0, 0);
        }
        
        return $start;
    }
    
    public function getEnd() {
        $end = null;
        foreach ($this->getCallsheets() as $callsheet) {
            foreach ($callsheet->getEvents() as $event) {
                if (!$end || $end < $event->getEnd()) {
                    $end = $event->getEnd();
                }
            }
            foreach ($callsheet->getShotlists() as $shotlist) {
                foreach ($shotlist->getEvents() as $event) {
                    if (!$end || $end < $event->getEnd()) {
                        $end = $event->getEnd();
                    }
                }
            }
        }
        
        if(!$end) {
            $end = $this->getCreated();
            $end->setTime(0, 0, 0);
        }
        
        return $end;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $callsheets;

    /**
     * Constructor
     */
    public function __construct() {
        $this->callsheets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add callsheets
     *
     * @param \AppBundle\Entity\Project\Callsheet $callsheets
     * @return Project
     */
    public function addCallsheet(\AppBundle\Entity\Project\Callsheet $callsheets) {
        $this->callsheets[] = $callsheets;

        return $this;
    }

    /**
     * Remove callsheets
     *
     * @param \AppBundle\Entity\Project\Callsheet $callsheets
     */
    public function removeCallsheet(\AppBundle\Entity\Project\Callsheet $callsheets) {
        $this->callsheets->removeElement($callsheets);
    }

    /**
     * Get callsheets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCallsheets() {
        return $this->callsheets;
    }

}
