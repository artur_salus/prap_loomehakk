<?php

namespace AppBundle\Entity\Project;

use Doctrine\ORM\Mapping as ORM;

/**
 * Callsheet
 */
class Callsheet
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Callsheet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add events
     *
     * @param \AppBundle\Entity\Project\CallsheetEvent $events
     * @return Callsheet
     */
    public function addEvent(\AppBundle\Entity\Project\CallsheetEvent $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \AppBundle\Entity\Project\CallsheetEvent $events
     */
    public function removeEvent(\AppBundle\Entity\Project\CallsheetEvent $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $shotlists;

    /**
     * @var \AppBundle\Entity\Project\Project
     */
    private $project;


    /**
     * Add shotlists
     *
     * @param \AppBundle\Entity\Project\Shotlist $shotlists
     * @return Callsheet
     */
    public function addShotlist(\AppBundle\Entity\Project\Shotlist $shotlists)
    {
        $this->shotlists[] = $shotlists;

        return $this;
    }

    /**
     * Remove shotlists
     *
     * @param \AppBundle\Entity\Project\Shotlist $shotlists
     */
    public function removeShotlist(\AppBundle\Entity\Project\Shotlist $shotlists)
    {
        $this->shotlists->removeElement($shotlists);
    }

    /**
     * Get shotlists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShotlists()
    {
        return $this->shotlists;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project\Project $project
     * @return Callsheet
     */
    public function setProject(\AppBundle\Entity\Project\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
}
