<?php

namespace AppBundle\Entity\Project;

class EventPerson extends Person
{
    
    /**
     * @var string
     */
    private $role;

    /**
     * @var \AppBundle\Entity\Project\Event
     */
    private $event;


    /**
     * Set role
     *
     * @param string $role
     * @return EventPerson
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set event
     *
     * @param \AppBundle\Entity\Project\Event $event
     * @return EventPerson
     */
    public function setEvent(\AppBundle\Entity\Project\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \AppBundle\Entity\Project\Event 
     */
    public function getEvent()
    {
        return $this->event;
    }
}
