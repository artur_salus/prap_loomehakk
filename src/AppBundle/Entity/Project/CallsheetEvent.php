<?php

namespace AppBundle\Entity\Project;

class CallsheetEvent extends Event
{
    
    /**
     * @var \AppBundle\Entity\Project\Callsheet
     */
    private $callsheet;


    /**
     * Set callsheet
     *
     * @param \AppBundle\Entity\Project\Callsheet $callsheet
     * @return CallsheetEvent
     */
    public function setCallsheet(\AppBundle\Entity\Project\Callsheet $callsheet = null)
    {
        $this->callsheet = $callsheet;

        return $this;
    }

    /**
     * Get callsheet
     *
     * @return \AppBundle\Entity\Project\Callsheet 
     */
    public function getCallsheet()
    {
        return $this->callsheet;
    }
}
