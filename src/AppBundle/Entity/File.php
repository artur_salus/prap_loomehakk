<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File as SymfonyFile;

/**
 * File
 */
class File {

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;
    protected $file;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }

    public function setFile(SymfonyFile $file) {
        return $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }

    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        $this->getFile()->move($this->getUploadRootDir(), $this->id . '.' . $this->getFile()->guessExtension());
        $this->path = $this->getFile()->getClientOriginalName();
        $this->setFile(null);
    }
    
    public function getUrl() {
        return str_replace(DIRECTORY_SEPARATOR, '/', str_replace($this->getRootDir(), '', $this->getPath()));
    }
    
    protected function getUploadRootDir() {
         return $this->getRootDir().'/web';
    }
    
    protected function getRootDir() {
        return realpath(__DIR__.'/../../../web/');
    }

}
